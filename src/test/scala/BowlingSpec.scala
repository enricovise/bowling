import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec

class BowlingSpec extends AnyFunSpec with GivenWhenThen {

  describe("A game") {
    it("should have a score") {
      Given("an empty sequence of rolls")
      var rolls = List[Int]()

      When("the 1st frame is entered")
      rolls ++= List(8, 2)
      Then("the score of the game is the score of the 1s frame")
      assert(new Knocked(rolls).getScore() == 10)

      When("the 2nd frame is entered")
      rolls ++= List(5, 4)
      Then("the score of the game is the score of the first 2 frames")
      assert(new Knocked(rolls).getScore() == 15 + 9)

      When("the 3rd frame is entered")
      rolls ++= List(9, 0)
      Then("the score of the game is the score of the first 2 frames")
      assert(new Knocked(rolls).getScore() == 15 + 9 + 9)

      When("the 4th frame is entered")
      rolls ++= List(10)
      Then("the score of the game is the score of the first 2 frames")
      assert(new Knocked(rolls).getScore() == 15 + 9 + 9 + 10)

      When("the 5th frame is entered")
      rolls ++= List(10)
      Then("the score of the game is the score of the first 2 frames")
      assert(new Knocked(rolls).getScore() == 15 + 9 + 9 + 10 + 10)

      When("the 6th frame is entered")
      rolls ++= List(5, 5)
      Then("the score of the game is the score of the first 2 frames")
      assert(new Knocked(rolls).getScore() == 15 + 9 + 9 + 25 + 20 + 10)

      When("the 7th frame is entered")
      rolls ++= List(5, 3)
      Then("the score of the game is the score of the first 2 frames")
      assert(new Knocked(rolls).getScore() == 15 + 9 + 9 + 25 + 20 + 15 + 8)

      When("the 8th frame is entered")
      rolls ++= List(6, 3)
      Then("the score of the game is the score of the first 2 frames")
      assert(new Knocked(rolls).getScore() == 15 + 9 + 9 + 25 + 20 + 15 + 8 + 9)

      When("the 9th frame is entered")
      rolls ++= List(9, 1)
      Then("the score of the game is the score of the first 2 frames")
      assert(new Knocked(rolls).getScore() == 15 + 9 + 9 + 25 + 20 + 15 + 8 + 9 + 10)

      When("the 10th frame is entered")
      rolls ++= List(9, 1, 10)
      Then("the score of the game is the score of the first 2 frames")
      assert(new Knocked(rolls).getScore() == 15 + 9 + 9 + 25 + 20 + 15 + 8 + 9 + 19 + 20)
    }
  }

  describe("a perfect game") {
    it("should score 300") {
      assert(new Knocked(Seq.fill(12)(10)).getScore() == 300)
    }
  }

  describe("a no-roll game") {
    it("should score 0") {
      assert(Knocked().getScore() == 0)
    }
  }

  describe("a single roll, or a single frame with only one roll") {
    it("should score the score of the roll") {
      val pins = 10
      assert(new Knocked(List(pins)).getScore() == pins)
    }
  }

  describe("two rolls, or a single frame with two rolls") {
    it("should score the sum of the scores of the rolls") {
      val frame = List(0, 10)
      assert(new Knocked(frame).getScore() == frame.sum)
    }
  }

  describe("a strike") {
    it("should score 10 plus the score of the following two rolls") {
      Given("a strike")
      val strike = List(10)
      And("two rolls")
      val twoRolls = List(4, 5)

      When("the current frame is not the last one")
      val firstFrameNumber = 0
      Then("the total score is 10 plus twice the score of the two rolls")
      assert(new Knocked(strike ++: twoRolls).getScore(firstFrameNumber) == new Knocked(strike).getScore() + 2 * new Knocked(twoRolls).getScore())

      When("the current frame is the last one")
      val lastFrameNumber = 9
      Then("the total score is 10 plus the score of the two rolls")
      assert(new Knocked(strike ++: twoRolls).getScore(lastFrameNumber) == new Knocked(strike).getScore() + new Knocked(twoRolls).getScore())
    }
  }

  describe("a spare") {
    it("should score 10 plus the score of the following roll") {
      Given("a spare")
      val spare = List(0, 10)
      And("a roll")
      val roll = List(2)

      When("the current frame is not the last one")
      val firstFrameNumber = 0
      Then("the total score is 10 plus twice the score of the roll")
      assert(new Knocked(spare ++: roll).getScore(firstFrameNumber) == new Knocked(spare).getScore() + 2 * new Knocked(roll).getScore())

      When("the current frame is the last one")
      val lastFrameNumber = 9
      Then("the total score is 10 plus the score of the roll")
      assert(new Knocked(spare ++: roll).getScore(lastFrameNumber) == new Knocked(spare).getScore() + new Knocked(roll).getScore())
    }
  }

}
