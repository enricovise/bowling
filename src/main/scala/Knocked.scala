class Knocked(pins: Seq[Int] = Seq()) {

  private def strikeScore(b: Int, c: Int): Int = 10 + b + c

  private def standardScore(a: Int, b: Int, c: Int = 0): Int = a + b + (if ((a + b) == 10) c else 0)

  def getScore(frameNumber: Int = 0): Int = this.pins match {
    case Nil => 0
    case a +: Nil => a
    case a +: b +: Nil => a + b
    case 10 +: b +: c +: Nil if frameNumber >= 9 => this.strikeScore(b, c)
    case 10 +: b +: c +: tail => this.strikeScore(b, c) + new Knocked(b +: c +: tail).getScore(frameNumber + 1)
    case a +: b +: c +: Nil if frameNumber >= 9 => this.standardScore(a, b, c)
    case a +: b +: c +: tail => this.standardScore(a, b, c) + new Knocked(c +: tail).getScore(frameNumber + 1)
  }

}

object Knocked {
  def apply(manyInts: Int*): Knocked = {
    new Knocked(manyInts.toList)
  }
}
